pvlogger library. To use, use add the following snippet to your dockerfile:

**X86_64 (static binary)**
```
FROM registry.gitlab.com/pantavor/pantavisor-runtime/pvlogger:X86_64-master as pvlogger

COPY --chown=0:0 --from pvlogger /usr/local/bin/pvlogger /usr/local/bin/pvlogger
```

**ARM 32-bit (static binary)**
```
FROM registry.gitlab.com/pantavor/pantavisor-runtime/pvlogger:ARM32V6-master as pvlogger

COPY --chown=0:0 --from pvlogger /usr/local/bin/pvlogger /usr/local/bin/pvlogger
```
